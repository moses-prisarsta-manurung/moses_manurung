import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/api/order_api.dart';
import 'package:jubelio/business_logic/api/product_api.dart';
import 'package:jubelio/business_logic/controller/book_counter.dart';
import 'package:jubelio/business_logic/controller/order_controller.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/business_logic/controller/sale_controller.dart';
import 'package:jubelio/ui/pages/root_page.dart';

void main() {
  runApp(const MyApp());
  Get.put(BookCounter());
  Get.lazyPut(() => ProductConnect());
  Get.lazyPut(() => ProductController());
  Get.lazyPut(() => ProductSaleController());
  Get.lazyPut(() => OrderConnect());
  Get.lazyPut(() => OrderController());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Jubelio',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: RootPage(),
    );
  }
}
