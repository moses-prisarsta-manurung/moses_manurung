import 'package:get/get.dart';
import 'package:jubelio/business_logic/api/pagination.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'dart:convert';

class ProductConnect extends GetConnect {
  @override
  void onInit() {
    httpClient.baseUrl =
        'https://codetesting.jubelio.store/wp-json/wc/v3/products';
    super.onInit();
  }

  String username = 'ck_1cbb2c1902d56b629cd9a555cc032c4b478b26ce';
  String password = 'cs_7be10f0328c5b1d6a1a3077165b226af71d8b9dc';
  String basicAuth = 'Basic ' +
      base64.encode(utf8.encode(
          'ck_1cbb2c1902d56b629cd9a555cc032c4b478b26ce:cs_7be10f0328c5b1d6a1a3077165b226af71d8b9dc'));

  Future getProducts(PaginationFilter filter) async {
    final Response<List<dynamic>> response = await get(
        '?order=asc&&page=${filter.page}&per_page=${filter.limit}',
        headers: {"Authorization": basicAuth});
    // print(response.body);
    if (response.body == null) {
      return [];
    } else {
      return response.isOk
          ? (response.body as List?)
                  ?.map((e) => ProductModel.fromJSON(e))
                  .toList() ??
              []
          : [];
    }
  }

  Future getProductsSale(PaginationFilter filter) async {
    final Response<List<dynamic>> response = await get(
        '?order=asc&&page=${filter.page}&per_page=${filter.limit}',
        headers: {"Authorization": basicAuth});
    // print(response.body);
    if (response.body == null) {
      return [];
    } else {
      return response.isOk
          ? (response.body as List?)
                  ?.map((e) => ProductModel.fromJSON(e))
                  .toList() ??
              []
          : [];
    }
  }

  Future getProductDetail(int id) async {
    final Response<Map<String, dynamic>> response =
        await get('/${id}', headers: {"Authorization": basicAuth});
    // print(response.body);
    if (response.body == null) {
      return [];
    } else {
      return ProductModel.fromJSON(response.body!);
    }
  }

  // Future<List<ProductModel>> getProducts() async {
  //   final Response<Map<String, dynamic>> response =
  //       await get('products', headers: {"Authorization": basicAuth});
  //   print(response.body);
  //   return response.isOk
  //       ? (response.body as List?)
  //               ?.map((e) => ProductModel.fromJSON(e))
  //               .toList() ??
  //           []
  //       : [];
  // }

  // Future<List<ProductModel>> getProducts() async {
  //   final Response<List<dynamic>> response =
  //       await get('products?order=asc', headers: {"Authorization": basicAuth});
  //   print(response.body);
  //   return response.isOk
  //       ? (response.body as List?)
  //               ?.map((e) => ProductModel.fromJSON(e))
  //               .toList() ??
  //           []
  //       : [];
  // }

  Future getProductBySearch(String keyword) async {
    final Response<List<dynamic>> response =
        await get('?search=$keyword', headers: {"Authorization": basicAuth});
    if (response.body == null) {
      return [];
    } else {
      return response.isOk
          ? (response.body as List?)
                  ?.map((e) => ProductModel.fromJSON(e))
                  .toList() ??
              []
          : [];
    }
  }
}
