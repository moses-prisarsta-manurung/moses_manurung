import 'package:get/get.dart';
import 'package:jubelio/business_logic/api/pagination.dart';
import 'dart:convert';

import 'package:jubelio/business_logic/model/order_model.dart';

class OrderConnect extends GetConnect {
  @override
  void onInit() {
    httpClient.baseUrl =
        'https://codetesting.jubelio.store/wp-json/wc/v3/orders';
    super.onInit();
  }

  String username = 'ck_1cbb2c1902d56b629cd9a555cc032c4b478b26ce';
  String password = 'cs_7be10f0328c5b1d6a1a3077165b226af71d8b9dc';
  String basicAuth = 'Basic ' +
      base64.encode(utf8.encode(
          'ck_1cbb2c1902d56b629cd9a555cc032c4b478b26ce:cs_7be10f0328c5b1d6a1a3077165b226af71d8b9dc'));

  Future getProducts(PaginationFilter filter) async {
    final Response<List<dynamic>> response = await get(
        '?order=asc&&page=${filter.page}&per_page=${filter.limit}',
        headers: {"Authorization": basicAuth});
    // print(response.body);
    if (response.body == null) {
      return [];
    } else {
      return response.isOk
          ? (response.body as List?)
                  ?.map((e) => OrderModel.fromJSON(e))
                  .toList() ??
              []
          : [];
    }
  }

  Future getProductDetail(int id) async {
    final Response<Map<String, dynamic>> response =
        await get('/${id}', headers: {"Authorization": basicAuth});
    // print(response.body);
    if (response.body == null) {
      return [];
    } else {
      return OrderModel.fromJSON(response.body!);
    }
  }
}
