class PaginationFilter {
  late int page, limit;

  PaginationFilter([this.page = 1, this.limit = 5]);

  @override
  String toString() => 'page: $page' 'limit: $limit';

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;
    return o is PaginationFilter && o.page == page && o.limit == limit;
  }

  @override
  int get hashCode => page.hashCode ^ limit.hashCode;
}
