import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/api/order_api.dart';
import 'package:jubelio/business_logic/api/pagination.dart';
import 'package:jubelio/business_logic/model/order_model.dart';

class OrderController extends GetxController {
  final RxList<OrderModel> listOrders = RxList();
  final OrderConnect orderConnect = Get.find();
  var isDataLoading = false.obs;

  final paginationFilter = PaginationFilter().obs;
  int get limit => paginationFilter.value.limit;
  int get page => paginationFilter.value.page;
  RxBool get lastPage => false.obs;

  final keywordController = TextEditingController();
  var productNotFound = false.obs;

  @override
  void onInit() {
    ever(paginationFilter, (_) => productList());
    changePaginationFilter(1, 5);
    super.onInit();
  }

  Future<void> productList() async {
    if (isDataLoading.value == false) {
      try {
        isDataLoading(true);
        var data = await orderConnect.getProducts(paginationFilter.value);
        if (data.isNotEmpty) {
          listOrders.value = data;
        } else {
          lastPage.value = true;
        }
      } finally {
        isDataLoading(false);
      }
    }
  }

  void changePaginationFilter(int page, int limit) {
    paginationFilter.update((val) {
      val!.page = page;
      val.limit = limit;
    });
  }

  void loadNextPage() {
    changePaginationFilter(page, limit + 5);
  }

  Future detailProduct(int id) async {
    isDataLoading(true);
    final data = await orderConnect.getProductDetail(id);
    isDataLoading(false);
    return data;
  }
}
