import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/api/product_api.dart';
import 'package:jubelio/business_logic/api/pagination.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/business_logic/model/product_model.dart';

class ProductSaleController extends GetxController {
  final RxList<ProductModel> listProductSale = RxList();
  final ProductConnect productConnect = Get.find();
  var isDataLoading = false.obs;

  final paginationFilter = PaginationFilter().obs;
  int get limit => paginationFilter.value.limit;
  int get page => paginationFilter.value.page;
  RxBool get lastPage => false.obs;

  final keywordController = TextEditingController();
  var productNotFound = false.obs;

  @override
  void onInit() {
    ever(paginationFilter, (_) => productList());
    changePaginationFilter(1, 100);
    super.onInit();
  }

  // Future productList() async {
  //   isDataLoading(true);
  //   final List<ProductModel> data = await productConnect.getProducts();
  //   if (data.isNotEmpty) {
  //     listProducts.value = data;
  //   }
  //   print(listProducts.value);
  // }

  Future<void> productList() async {
    if (isDataLoading.value == false) {
      try {
        isDataLoading(true);
        var data = await productConnect.getProducts(paginationFilter.value);
        if (data.isNotEmpty) {
          listProductSale.value = data;
        } else {
          lastPage.value = true;
        }
      } finally {
        isDataLoading(false);
      }
    }
  }

  void changePaginationFilter(int page, int limit) {
    paginationFilter.update((val) {
      val!.page = page;
      val.limit = limit;
    });
  }

  void loadNextPage() {
    changePaginationFilter(page, limit + 5);
  }

  // Future detailProduct(int id) async {
  //   if (isDataLoading.value == false) {
  //     try {
  //       isDataLoading(true);
  //       var data = await productConnect.getProductDetail(id);
  //       if (data.isNotEmpty) {
  //         listProducts.value = data;
  //       }
  //     } finally {
  //       isDataLoading(false);
  //     }
  //   }
  // }

  Future detailProduct(int id) async {
    isDataLoading(true);
    final data = await productConnect.getProductDetail(id);
    isDataLoading(false);
    return data;
  }

  Future relatedProducts(int id) async {
    isDataLoading(true);
    final data = await productConnect.getProductDetail(id);
    isDataLoading(false);
    return data;
  }
}
