import 'package:get/get.dart';

class BookCounter extends GetxController {
  RxInt counter = 0.obs;

  void add() {
    if (counter >= 0) counter.value++;
  }

  void subtract() {
    if (counter > 0) counter.value--;
  }
}
