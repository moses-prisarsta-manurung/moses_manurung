import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:jubelio/business_logic/api/product_api.dart';
import 'package:jubelio/business_logic/api/pagination.dart';
import 'package:jubelio/business_logic/model/product_model.dart';

class ProductController extends GetxController {
  late RxList<ProductModel> listProducts = RxList();
  final RxList<ProductModel> listProductBySearch = RxList();
  final RxList<ProductModel> listRelated = RxList();
  final ProductConnect productConnect = Get.find();
  var isDataLoading = false.obs;
  final GetStorage box = GetStorage();
  static const String localKey = 'store';

  final paginationFilter = PaginationFilter().obs;
  int get limit => paginationFilter.value.limit;
  int get page => paginationFilter.value.page;
  RxBool get lastPage => false.obs;

  final keywordController = TextEditingController();
  var productNotFound = false.obs;

  @override
  void onInit() {
    // final List localData = box.read<List>(localKey) ?? [];
    // listProducts =
    //     RxList(localData.map((e) => ProductModel.fromJSON(e)).toList());
    ever(paginationFilter, (_) => productList());
    changePaginationFilter(1, 5);
    super.onInit();
  }

  Future<void> productList() async {
    if (isDataLoading.value == false) {
      try {
        isDataLoading(true);
        var data = await productConnect.getProducts(paginationFilter.value);
        if (data.isNotEmpty) {
          listProducts.value = data;
        } else {
          lastPage.value = true;
        }
      } finally {
        isDataLoading(false);
      }
    }
  }

  Future<void> updateLocalData(List<ProductModel> data) async {
    await box.write(localKey, data.map((e) => e.toJSON).toList());
  }

  // Future<void> productList() async {
  //   if (isDataLoading.value == false) {
  //     try {
  //       isDataLoading(true);
  //       var data = await productConnect.getProducts(paginationFilter.value);
  //       if (data.isNotEmpty) {
  //         listProducts.value = data;
  //       } else {
  //         lastPage.value = true;
  //       }
  //     } finally {
  //       isDataLoading(false);
  //     }
  //   }
  // }

  void changePaginationFilter(int page, int limit) {
    paginationFilter.update((val) {
      val!.page = page;
      val.limit = limit;
    });
  }

  void loadNextPage() {
    changePaginationFilter(page, limit + 5);
  }

  Future detailProduct(int id) async {
    isDataLoading(true);
    final data = await productConnect.getProductDetail(id);
    isDataLoading(false);
    return data;
  }

  Future relatedProducts(int id) async {
    isDataLoading(true);
    final data = await productConnect.getProductDetail(id);
    isDataLoading(false);
    return data;
  }

  Future producListtBySearch(String keyword) async {
    isDataLoading(true);
    if (keyword.isNotEmpty) {
      var data = await productConnect.getProductBySearch(keyword);
      if (data.isNotEmpty) {
        listProductBySearch.value = data;
        productNotFound.value = false;
      } else {
        productNotFound.value = true;
      }
      isDataLoading(false);
      return data;
    } else {
      listProductBySearch.value = [];
      var message = 'Product is not found';
      return message;
    }
  }
}
