import 'package:jubelio/utils/utils.dart';

class OrderModel {
  late int id;
  late String status,
      date_created,
      total,
      order_key,
      payment_method,
      payment_method_title;
  late bool needs_payment;
  late List<Items> line_items;
  late Billing billing;

  OrderModel(
      {required this.id,
      required this.status,
      required this.date_created,
      required this.total,
      required this.order_key,
      required this.payment_method,
      required this.payment_method_title,
      required this.needs_payment,
      required this.line_items,
      required this.billing});

  OrderModel.fromJSON(JSON json) {
    id = json['id'];
    status = json['status'];
    date_created = json['date_created'];
    total = json['total'];
    order_key = json['order_key'];
    payment_method = json['payment_method'];
    payment_method_title = json['payment_method_title'];
    needs_payment = json['needs_payment'];
    line_items = json['line_items'] == null
        ? []
        : (json['line_items'] as List).map((e) => Items.fromJSON(e)).toList();
    billing = Billing.fromJSON(json['billing']);
  }
}

class Items {
  late int id, quantity;
  late num price;
  late String? name, subtotal, total, subtotal_tax, total_tax;
  late OrderImage image;

  Items(
      {required this.id,
      required this.quantity,
      required this.price,
      required this.name,
      required this.subtotal,
      required this.total,
      required this.subtotal_tax,
      required this.total_tax,
      required this.image});

  Items.fromJSON(JSON json) {
    id = json['id'];
    quantity = json['quantity'];
    price = json['price'];
    name = json['name'];
    subtotal = json['subtotal'];
    total = json['total'];
    subtotal_tax = json['subtotal_tax'];
    total_tax = json['total_tax'];
    image = OrderImage.fromJSON(json['image']);
  }
}

class OrderImage {
  late String src, id;

  OrderImage({required this.src, required this.id});

  OrderImage.fromJSON(JSON json) {
    src = json['src'];
    id = json['id'];
  }
  JSON get toJSON => {'src': src};
}

class Billing {
  late String first_name,
      last_name,
      company,
      address_1,
      address_2,
      city,
      state,
      postcode,
      country,
      email,
      phone;

  Billing(
      {required this.first_name,
      required this.last_name,
      required this.company,
      required this.address_1,
      required this.address_2,
      required this.city,
      required this.state,
      required this.postcode,
      required this.country,
      required this.email,
      required this.phone});

  Billing.fromJSON(JSON json) {
    first_name = json['first_name'];
    last_name = json['last_name'];
    company = json['company'];
    address_1 = json['address_1'];
    address_2 = json['address_2'];
    city = json['city'];
    state = json['state'];
    postcode = json['postcode'];
    country = json['country'];
    company = json['company'];
    email = json['email'];
    phone = json['phone'];
  }
}
