import '../../utils/utils.dart';

class ProductModel {
  late int id, rating_count, total_sales;
  late String name,
      sale_price,
      price_html,
      short_description,
      description,
      price;
  late bool on_sale;
  late List<int> related_ids, variations;
  late List<Img> images;
  late List<Categories> categories;
  late List<Attributes> attributes;
  late List<DefaultAttributes> default_attributes;

  ProductModel(
      {required this.id,
      required this.rating_count,
      required this.total_sales,
      required this.name,
      required this.sale_price,
      required this.on_sale,
      required this.price_html,
      required this.short_description,
      required this.price,
      required this.description,
      required this.related_ids,
      required this.variations,
      required this.images,
      required this.categories,
      required this.attributes,
      required this.default_attributes});

  ProductModel.fromJSON(JSON json) {
    id = json['id'];
    rating_count = json['rating_count'];
    total_sales = json['total_sales'];
    name = json['name'];
    sale_price = json['sale_price'];
    on_sale = json['on_sale'];
    price_html = json['price_html'];
    price = json['price'];
    short_description = json['short_description'];
    description = json['description'];
    related_ids = json['related_ids'] == null
        ? []
        : (json['related_ids'] as List)
            .map((e) => int.parse(e.toString()))
            .toList();
    variations = json['variations'] == null
        ? []
        : (json['variations'] as List)
            .map((e) => int.parse(e.toString()))
            .toList();
    images = json['images'] == null
        ? []
        : (json['images'] as List).map((e) => Img.fromJSON(e)).toList();
    categories = json['categories'] == null
        ? []
        : (json['categories'] as List)
            .map((e) => Categories.fromJSON(e))
            .toList();
    attributes = json['attributes'] == null
        ? []
        : (json['attributes'] as List)
            .map((e) => Attributes.fromJSON(e))
            .toList();
    default_attributes = json['default_attributes'] == null
        ? []
        : (json['default_attributes'] as List)
            .map((e) => DefaultAttributes.fromJSON(e))
            .toList();
  }

  JSON get toJSON => {
        'id': id,
        'total_sales': total_sales,
        'name': name,
        'sale_price': sale_price,
        'on_sale': on_sale,
        'price_html': price_html,
        'short_description': short_description,
        'images': images,
        'categories': categories,
        'attributes': attributes,
      };
}

class Img {
  late String src, name;

  Img({required this.src, required this.name});

  Img.fromJSON(JSON json) {
    src = json['src'];
    name = json['name'];
  }

  JSON get toJSON => {'src': src, 'name': name};
}

class Categories {
  late String name;
  late int id;

  Categories({required this.name, required this.id});

  Categories.fromJSON(JSON json) {
    name = json['name'];
    id = json['id'];
  }

  JSON get toJSON => {'name': name, 'id': id};
}

class Attributes {
  late String name;
  late List<String> options;
  late int id;

  Attributes({required this.name, required this.options, required this.id});

  Attributes.fromJSON(JSON json) {
    name = json['name'];
    options = json['options'] == null
        ? []
        : (json['options'] as List).map((e) => e.toString()).toList();
    id = json['id'];
  }

  JSON get toJSON => {'name': name, 'id': id, 'options': options};
}

class DefaultAttributes {
  late String? name, options;
  late int id;

  DefaultAttributes(
      {required this.name, required this.options, required this.id});

  DefaultAttributes.fromJSON(JSON json) {
    name = json['name'];
    options = json['options'];
    id = json['id'];
  }

  JSON get toJSON => {'name': name, 'id': id, 'options': options};
}
