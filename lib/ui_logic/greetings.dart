import 'package:flutter/material.dart';

String greetingMessage() {
  var timeNow = TimeOfDay.now().hour;

  if (timeNow < 12) {
    return 'Good Morning';
  } else if (timeNow >= 12 && timeNow < 18) {
    return 'Good Afternoon';
  } else {
    return 'Good Evening';
  }
}
