import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/ui/pages/details_page.dart';
import 'package:jubelio/utils/theme.dart';

class ListProduct extends StatelessWidget {
  const ListProduct({super.key});

  @override
  Widget build(_) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Obx(
        () => Column(
          children: [
            for (final ProductModel products
                in Get.find<ProductController>().listProducts.toList())
              InkWell(
                onTap: () async {
                  final data = await Get.find<ProductController>()
                      .detailProduct(products.id);
                  if (data == null) return;
                  late ProductModel related;
                  for (var i in products.related_ids) {
                    related =
                        await Get.find<ProductController>().relatedProducts(i);
                  }
                  Get.to(() => DetailsPage(data, related));
                },
                child: Hero(
                  tag: "${products.id}home",
                  child: Padding(
                    padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 0),
                    child: Container(
                      width: Get.width,
                      height: Get.height * 0.14,
                      decoration: AppTheme.boxdecoration1,
                      child: Padding(
                        padding:
                            const EdgeInsetsDirectional.fromSTEB(10, 10, 5, 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12),
                              child: Container(
                                color: AppTheme.color2,
                                child: Image.network(
                                    (products.images.isNotEmpty)
                                        ? products.images[0].src
                                        : 'notfound',
                                    width: Get.width * 0.24,
                                    height: Get.height * 0.24,
                                    fit: BoxFit.cover,
                                    errorBuilder: (_, error, stackTrace) {
                                  return Image.asset(
                                    'assets/nophoto1.png',
                                    width: Get.width * 0.24,
                                    height: Get.height * 0.24,
                                    fit: BoxFit.cover,
                                  );
                                }),
                              ),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsetsDirectional.fromSTEB(
                                    12, 0, 0, 0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(products.name,
                                          softWrap: false,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: AppTheme.text18Bold),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 4.0, top: 4, bottom: 4),
                                      child: Row(
                                        children: [
                                          for (final i in products.categories)
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  right: 4.0),
                                              child: Container(
                                                decoration:
                                                    AppTheme.boxdecoration2,
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(2),
                                                  child: Text(
                                                    i.name,
                                                    style: AppTheme.text12Bold,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          (products.on_sale == true)
                                              ? Container(
                                                  decoration:
                                                      AppTheme.boxdecoration3,
                                                  child: const Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            3, 1.5, 3, 1.5),
                                                    child: Text('Sale!!!',
                                                        style: AppTheme
                                                            .text14Bold),
                                                  ))
                                              : const Text('')
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(4),
                                      child: (products.price_html != '')
                                          ? HtmlWidget(products.price_html,
                                              textStyle: AppTheme.text14)
                                          : Text(
                                              (products.sale_price != '')
                                                  ? products.sale_price
                                                  : '1000',
                                              style: AppTheme.text14),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.star_rate,
                                    color: AppTheme.color8,
                                  ),
                                  Text(products.rating_count.toString())
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}
