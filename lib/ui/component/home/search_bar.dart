import 'package:anim_search_bar/anim_search_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/ui/pages/search_page.dart';
import 'package:jubelio/utils/theme.dart';

class SearchBar extends GetView<ProductController> {
  const SearchBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 10, left: 10),
          child: AnimSearchBar(
            style: TextStyle(color: AppTheme.color3),
            rtl: true,
            autoFocus: true,
            textFieldColor: AppTheme.color8,
            searchIconColor: AppTheme.color3,
            color: AppTheme.color8,
            textFieldIconColor: AppTheme.color3,
            width: Get.width * 0.945,
            helpText: 'Search...',
            textController: controller.keywordController,
            onSuffixTap: () {},
            onSubmitted: (p0) async {
              final keyword = controller.keywordController;
              if (keyword.text == '') {
                return;
              } else {
                await Get.find<ProductController>()
                    .producListtBySearch(keyword.text);
                Get.to(() => SearchPage(keyword));
              }
            },
          ),
        )
      ],
    );
  }
}
