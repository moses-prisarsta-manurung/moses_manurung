import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/basic.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/business_logic/controller/sale_controller.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/ui/pages/details_page.dart';
import 'package:jubelio/utils/theme.dart';

class NewListProduct extends StatelessWidget {
  const NewListProduct({super.key});

  @override
  Widget build(_) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Obx(
        () => ListView(
          scrollDirection: Axis.horizontal,
          children: [
            for (final ProductModel products
                in Get.find<ProductSaleController>().listProductSale.toList())
              (products.on_sale == true)
                  ? InkWell(
                      onTap: () async {
                        final data = await Get.find<ProductController>()
                            .detailProduct(products.id);
                        if (data == null) return;
                        late ProductModel related;
                        for (var i in products.related_ids) {
                          related = await Get.find<ProductController>()
                              .relatedProducts(i);
                        }
                        Get.to(() => DetailsPage(data, related));
                      },
                      child: Hero(
                        tag: "newList${products.id}",
                        child: Card(
                          child: new Container(
                            width: 220.0,
                            child: Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  10, 10, 10, 10),
                              child: Expanded(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(12),
                                      child: Container(
                                        color: AppTheme.color2,
                                        child: Image.network(
                                            (products.images.isNotEmpty)
                                                ? products.images[0].src
                                                : 'notfound',
                                            width: 200,
                                            height: 200,
                                            fit: BoxFit.cover, errorBuilder:
                                                (_, error, stackTrace) {
                                          return Image.asset(
                                            'assets/nophoto1.png',
                                            width: 220,
                                            height: 200,
                                            fit: BoxFit.cover,
                                          );
                                        }),
                                      ),
                                    ),
                                    Text(products.name,
                                        softWrap: false,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: AppTheme.text18Bold),
                                    (products.price_html != '')
                                        ? HtmlWidget(products.price_html,
                                            textStyle: AppTheme.text14)
                                        : Text(
                                            (products.sale_price != '')
                                                ? products.sale_price
                                                : '1000',
                                            style: AppTheme.text14),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  : Container()
          ],
        ),
      ),
    );
  }
}
