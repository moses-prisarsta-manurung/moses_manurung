import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/utils/theme.dart';

class ConfirmContent extends StatelessWidget {
  const ConfirmContent(this.data, this.count, {super.key});
  final ProductModel data;
  final int count;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          InkWell(
            child: Container(
              // height: MediaQuery.of(context).size.height * 0.6,
              child: Image.asset(
                'assets/confirmation.gif',
                width: Get.width * 0.5,
              ),
            ),
          ),
          Card(
            child: Container(
              width: Get.width,
              height: Get.height * 0.19,
              child: Padding(
                padding: const EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Container(
                            color: AppTheme.color2,
                            child: Image.network(
                                (data.images.isNotEmpty)
                                    ? data.images[0].src
                                    : 'notfound',
                                width: Get.width * 0.24,
                                height: Get.height * 0.13,
                                fit: BoxFit.cover,
                                errorBuilder: (_, error, stackTrace) {
                              return Image.asset(
                                'assets/nophoto1.png',
                                width: Get.width * 0.24,
                                height: Get.height * 0.13,
                                fit: BoxFit.cover,
                              );
                            }),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(data.name,
                                  softWrap: false,
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                  style: AppTheme.text18Bold),
                              Text(
                                '$count x ${data.price}',
                                style: TextStyle(fontSize: 15),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Total Harga'),
                        Text(
                          'Rp${count * int.parse(data.price)}',
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10),
            child: Card(
              child: Container(
                width: Get.width,
                height: Get.height * 0.09,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Text(
                            'You use protection in this transaction',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Any problems? Check the policy and submit a claim through',
                          )
                        ],
                      ),
                      IconButton(
                          onPressed: () {},
                          icon: Icon(Icons.arrow_circle_right_outlined))
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 20),
            child: Card(
              child: Container(
                width: Get.width,
                height: Get.height * 0.3,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Shipping Information',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(right: 80.0),
                            child: Text(
                              'Kurir',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.w300),
                            ),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: const [
                              Text(
                                'SiCepat- Regular Package',
                              ),
                              Text(
                                '[Estimasi tiba 1-2 Mar 2023]',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 45.0),
                              child: Row(
                                children: const [
                                  Text(
                                    'No Resi ',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  Icon(
                                    Icons.copy,
                                    size: 15,
                                  )
                                ],
                              ),
                            ),
                            const Text(
                              '6986181648137590175917',
                              style: TextStyle(
                                  fontSize: 14, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 45.0),
                              child: Row(
                                children: const [
                                  Text(
                                    'Alamat ',
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w300),
                                  ),
                                  Icon(
                                    Icons.copy,
                                    size: 15,
                                  )
                                ],
                              ),
                            ),
                            Expanded(
                              child: const Text(
                                'Jl.Merdeka, Gg. Maju No.20, Bendungan Hilir, Tanah Abang, Jakarta Pusat, DKI Jakarta',
                                style: TextStyle(
                                    fontSize: 14, fontWeight: FontWeight.bold),
                                softWrap: false,
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
