import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:jubelio/ui/component/details/dialog.dart';
import 'package:jubelio/utils/theme.dart';

class BottomConfirmBar extends StatelessWidget {
  const BottomConfirmBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppTheme.color8),
      width: Get.width,
      height: Get.height * 0.07,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
            child: const Text(
              'Buy Now',
              style: TextStyle(
                  color: AppTheme.color3,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            onPressed: () {
              dialogSuccess(context);
            },
          ),
        ],
      ),
    );
  }
}
