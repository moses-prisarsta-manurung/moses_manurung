import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jubelio/business_logic/controller/order_controller.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/ui/pages/details_order_page.dart';
import 'package:jubelio/utils/theme.dart';

class ListOrder extends StatelessWidget {
  const ListOrder({super.key});

  @override
  Widget build(_) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Obx(
        () => Column(
          children: [
            InkWell(
              child: Container(
                // height: MediaQuery.of(context).size.height * 0.6,
                child: Image.asset(
                  'assets/order.gif',
                  width: Get.width * 0.65,
                ),
              ),
            ),
            for (final OrderModel orders
                in Get.find<OrderController>().listOrders.toList())
              Hero(
                tag: "order${orders.id}",
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                  child: Container(
                    width: Get.width,
                    height: Get.height * 0.24,
                    decoration: AppTheme.boxdecoration1,
                    child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              // ${DateFormat('dd/MM/yyyy').format(DateTime.parse(productList.departure_date)).toString()}
                              Text(DateFormat('dd/MM/yyyy')
                                  .format(DateTime.parse(orders.date_created))
                                  .toString()),
                              Container(
                                  decoration: AppTheme.boxdecoration3,
                                  child: Padding(
                                    padding:
                                        EdgeInsets.fromLTRB(3, 1.5, 3, 1.5),
                                    child: Text(orders.status,
                                        style: AppTheme.text14Bold),
                                  ))
                            ],
                          ),
                          Divider(
                            color: AppTheme.color1,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(12),
                                child: Container(
                                  color: AppTheme.color2,
                                  child: Image.network(
                                      (orders.line_items.isNotEmpty)
                                          ? orders.line_items[0].image.src
                                          : 'notfound',
                                      width: Get.width * 0.24,
                                      height: Get.height * 0.112,
                                      fit: BoxFit.cover,
                                      errorBuilder: (_, error, stackTrace) {
                                    return Image.asset(
                                      'assets/nophoto1.png',
                                      width: Get.width * 0.24,
                                      height: Get.height * 0.112,
                                      fit: BoxFit.cover,
                                    );
                                  }),
                                ),
                              ),
                              Expanded(
                                child: Padding(
                                  padding: const EdgeInsetsDirectional.fromSTEB(
                                      12, 0, 0, 0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Text(
                                            orders.line_items[0].name ?? '',
                                            softWrap: false,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: AppTheme.text18Bold),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Text(
                                            '${orders.line_items[0].quantity} Barang',
                                            style: TextStyle(fontSize: 14)),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Divider(
                            color: AppTheme.color1,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text('Spending Totals'),
                                  Text(
                                    orders.total,
                                    style: AppTheme.text18Bold,
                                  )
                                ],
                              ),
                              TextButton(
                                onPressed: () async {
                                  final data = await Get.find<OrderController>()
                                      .detailProduct(orders.id);
                                  if (data == null) return;
                                  Get.to(() => DetailsOrderPage(data));
                                },
                                child: Text('See Details'),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            // )
          ],
        ),
      ),
    );
  }
}
