import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:jubelio/utils/theme.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

void dialogNotSale(BuildContext context) {
  Alert(
      context: context,
      type: AlertType.warning,
      title: 'Hi Jubelio Lovers',
      desc:
          "Sorry, this product is not being sold again, please try to check other products",
      buttons: [
        DialogButton(
          gradient:
              const LinearGradient(colors: [AppTheme.color8, AppTheme.color8]),
          border: const Border.fromBorderSide(
            BorderSide(color: AppTheme.color8),
          ),
          child: const Text(
            'Close',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppTheme.color3),
          ),
          onPressed: (() => Get.back()),
        )
      ]).show();
  return;
}

void dialogSuccess(BuildContext context) {
  Alert(
      context: context,
      type: AlertType.success,
      title: 'Hi Jubelio Lovers',
      desc: "Transaction successfully added",
      buttons: [
        DialogButton(
          gradient:
              const LinearGradient(colors: [AppTheme.color8, AppTheme.color8]),
          border: const Border.fromBorderSide(
            BorderSide(color: AppTheme.color8),
          ),
          child: const Text(
            'Close',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: AppTheme.color3),
          ),
          onPressed: ((() => Get.back())),
        )
      ]).show();
  return;
}
