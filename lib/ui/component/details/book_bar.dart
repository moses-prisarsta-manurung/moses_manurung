import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/book_counter.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/ui/component/details/dialog.dart';
import 'package:jubelio/ui/pages/confirm_page.dart';
import 'package:jubelio/utils/theme.dart';

class BookBar extends GetView<BookCounter> {
  const BookBar(this.data, {super.key});

  final ProductModel data;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(color: AppTheme.color8),
      width: Get.width,
      height: Get.height * 0.07,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 6, 6, 6),
            child: Container(
              width: Get.width * 0.35,
              decoration: const BoxDecoration(
                  color: AppTheme.color3,
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              child: TextButton(
                child: const Text(
                  'Buy Now',
                  style: TextStyle(color: AppTheme.color1, fontSize: 18),
                ),
                onPressed: () {
                  if (data.on_sale == true) {
                    if (controller.counter.value == 0) {
                      return;
                    } else {
                      final count = controller.counter.value;
                      Get.to(() => ConfirmPage(data, count));
                    }
                  } else {
                    dialogNotSale(context);
                  }
                },
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(6, 6, 10, 6),
            child: Row(
              children: [
                Container(
                  width: Get.width * 0.12,
                  decoration: const BoxDecoration(
                      color: AppTheme.color3,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: TextButton(
                    child: const Text(
                      '-',
                      style: TextStyle(color: AppTheme.color1, fontSize: 18),
                    ),
                    onPressed: () {
                      controller.subtract();
                    },
                  ),
                ),
                Obx(() => Container(
                      alignment: Alignment.center,
                      width: Get.width * 0.12,
                      child: Text(
                        '${controller.counter}',
                        style: const TextStyle(
                          color: AppTheme.color3,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )),
                Container(
                  width: Get.width * 0.12,
                  decoration: const BoxDecoration(
                      color: AppTheme.color3,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: TextButton(
                    child: const Text(
                      '+',
                      style: TextStyle(color: AppTheme.color1, fontSize: 18),
                    ),
                    onPressed: () {
                      controller.add();
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
