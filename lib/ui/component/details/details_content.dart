import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/utils/theme.dart';

class DetailsContent extends StatelessWidget {
  const DetailsContent(this.data, this.related, {super.key});

  final ProductModel data, related;

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Container(
              color: AppTheme.color2,
              child: Image.network(
                  (data.images.isNotEmpty) ? data.images[0].src : 'notfound',
                  width: Get.width,
                  height: Get.height * 0.3,
                  fit: BoxFit.cover, errorBuilder: (_, error, stackTrace) {
                return Image.asset(
                  'assets/nophoto2.png',
                  width: Get.width,
                  height: Get.height * 0.3,
                  fit: BoxFit.cover,
                );
              }),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(12, 4, 12, 4),
          child: (data.price_html != '')
              ? HtmlWidget(data.price_html,
                  textStyle:
                      TextStyle(fontSize: 20, fontWeight: FontWeight.bold))
              : Text((data.sale_price != '') ? data.sale_price : '1000',
                  style: AppTheme.text14),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
          child: Text(
            data.name,
            style: const TextStyle(fontSize: 16),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 4, 12, 4),
          child: Row(
            children: [
              Text('Terjual ${data.total_sales}'),
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8),
                child: Container(
                    decoration: AppTheme.boxdecoration3,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(4, 1.5, 4, 1.5),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.star_rate,
                            size: 18,
                          ),
                          Text(' ${data.rating_count.toString()}',
                              style: AppTheme.text14Bold),
                        ],
                      ),
                    )),
              ),
              (data.on_sale == true)
                  ? Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Container(
                          decoration: AppTheme.boxdecoration3,
                          child: const Padding(
                            padding: EdgeInsets.fromLTRB(4, 3, 4, 3),
                            child: Text('Sale!!!', style: AppTheme.text14Bold),
                          )),
                    )
                  : const Text('')
            ],
          ),
        ),
        const Divider(
          color: AppTheme.color1,
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(12, 10, 12, 10),
          child: Text(
            'Product Detail',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: Row(
            children: [
              const Padding(
                padding: EdgeInsets.only(right: 40.0),
                child: Text(
                  'Category',
                  style: TextStyle(),
                ),
              ),
              for (final i in data.categories)
                Text(
                  '${i.name}, ',
                  style: TextStyle(),
                ),
            ],
          ),
        ),
        for (final i in data.attributes)
          (i != null && i.name == 'Brand')
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(12, 3, 12, 3),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 60.0),
                        child: Column(
                          children: const [
                            Text(
                              'Brand',
                              style: TextStyle(),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            i.options.toString(),
                            style: TextStyle(),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              : Padding(padding: const EdgeInsets.fromLTRB(12, 0, 12, 0)),
        for (final i in data.attributes)
          (i != null && i.name == 'Size')
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 70.0),
                        child: Column(
                          children: const [
                            Text(
                              'Size',
                              style: TextStyle(),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            i.options.toString(),
                            style: TextStyle(),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              : Padding(padding: const EdgeInsets.fromLTRB(12, 0, 12, 0)),
        for (final i in data.attributes)
          (i != null && i.name == 'Color')
              ? Padding(
                  padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 60.0),
                        child: Column(
                          children: const [
                            Text(
                              'Color',
                              style: TextStyle(),
                            ),
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            i.options.toString(),
                            style: TextStyle(),
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              : Padding(padding: const EdgeInsets.fromLTRB(12, 0, 12, 0)),
        const Divider(
          color: AppTheme.color1,
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(12, 10, 12, 10),
          child: Text(
            'Product Description',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: HtmlWidget(data.description),
        ),
        const Divider(
          color: AppTheme.color1,
        ),
        const Padding(
          padding: EdgeInsets.fromLTRB(12, 10, 12, 10),
          child: Text(
            'Related Product',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
        ),
        Column(
          children: [
            Column(children: [
              Hero(
                tag: "${related.id}details",
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(8, 8, 8, 0),
                  child: Container(
                    width: Get.width,
                    height: Get.height * 0.13,
                    decoration: AppTheme.boxdecoration1,
                    child: Padding(
                      padding:
                          const EdgeInsetsDirectional.fromSTEB(10, 10, 5, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Container(
                              color: AppTheme.color2,
                              child: Image.network(
                                  (related.images.isNotEmpty)
                                      ? related.images[0].src
                                      : 'notfound',
                                  width: Get.width * 0.25,
                                  height: Get.height * 0.25,
                                  fit: BoxFit.cover,
                                  errorBuilder: (_, error, stackTrace) {
                                return Image.asset(
                                  'assets/nophoto1.png',
                                  width: Get.width * 0.25,
                                  height: Get.height * 0.25,
                                  fit: BoxFit.cover,
                                );
                              }),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsetsDirectional.fromSTEB(
                                  12, 0, 0, 0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Text(related.name,
                                        softWrap: false,
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: AppTheme.text18Bold),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 4.0, top: 4, bottom: 4),
                                    child: Row(
                                      children: [
                                        for (final i in related.categories)
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                right: 4.0),
                                            child: Container(
                                              decoration:
                                                  AppTheme.boxdecoration2,
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(2),
                                                child: Text(
                                                  i.name,
                                                  style: AppTheme.text12Bold,
                                                ),
                                              ),
                                            ),
                                          ),
                                        (related.on_sale == true)
                                            ? Container(
                                                decoration:
                                                    AppTheme.boxdecoration3,
                                                child: const Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      3, 1.5, 3, 1.5),
                                                  child: Text('Sale!!!',
                                                      style:
                                                          AppTheme.text14Bold),
                                                ))
                                            : const Text('')
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(4),
                                    child: (related.price_html != '')
                                        ? HtmlWidget(related.price_html,
                                            textStyle: AppTheme.text14)
                                        : Text(
                                            (related.sale_price != '')
                                                ? related.sale_price
                                                : '1000',
                                            style: AppTheme.text14),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Icon(
                                  Icons.star_rate,
                                  color: AppTheme.color8,
                                ),
                                Text(related.rating_count.toString())
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ])
          ],
        ),
      ],
    );
  }
}
