import 'package:flutter/material.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/utils/theme.dart';

class PaymentDetails extends StatelessWidget {
  const PaymentDetails(this.data, {super.key});

  final OrderModel data;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15),
          child: Divider(
            color: AppTheme.color3,
            thickness: 5,
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Payment Details',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Metode Pemabayaran'),
                  Text(data.payment_method_title)
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 1.0, bottom: 1),
              child: Divider(
                color: AppTheme.color3,
                thickness: 2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Total Harga (${data.line_items[0].quantity} barang)'),
                  Text('${data.line_items[0].subtotal}'),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Tax'),
                  Text('${data.line_items[0].subtotal_tax}'),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8),
              child: Divider(
                color: AppTheme.color3,
                thickness: 2,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('Total Payment',
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
                  Text('${data.line_items[0].total}',
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.bold)),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
