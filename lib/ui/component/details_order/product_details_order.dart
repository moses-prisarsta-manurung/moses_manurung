import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/utils/theme.dart';

class ProductDetailsOrder extends StatelessWidget {
  const ProductDetailsOrder(this.data, {super.key});

  final OrderModel data;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(data.status,
            style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: AppTheme.color1)),
        Divider(
          color: AppTheme.color1,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 4.0),
          child: Row(
            children: [
              Text(
                '${data.order_key} ',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              Icon(
                Icons.copy,
                size: 15,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Purchase Date'),
              Text(DateFormat('dd/MM/yyyy')
                  .format(DateTime.parse(data.date_created))
                  .toString())
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15),
          child: Divider(
            color: AppTheme.color3,
            thickness: 5,
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Product Detail',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Card(
              child: Container(
                width: Get.width,
                height: Get.height * 0.2,
                child: Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(10, 10, 10, 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ClipRRect(
                            borderRadius: BorderRadius.circular(12),
                            child: Container(
                              color: AppTheme.color2,
                              child: Image.network(
                                  (data.line_items.isNotEmpty)
                                      ? data.line_items[0].image.src
                                      : 'notfound',
                                  width: Get.width * 0.24,
                                  height: Get.height * 0.13,
                                  fit: BoxFit.cover,
                                  errorBuilder: (_, error, stackTrace) {
                                return Image.asset(
                                  'assets/nophoto1.png',
                                  width: Get.width * 0.24,
                                  height: Get.height * 0.13,
                                  fit: BoxFit.cover,
                                );
                              }),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(data.line_items[0].name ?? '',
                                    softWrap: false,
                                    maxLines: 3,
                                    overflow: TextOverflow.ellipsis,
                                    style: AppTheme.text18Bold),
                                Text(
                                  '${data.line_items[0].quantity} x  ${data.line_items[0].price}',
                                  style: TextStyle(fontSize: 15),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text('Total Harga'),
                          Text(
                            'Rp ${data.line_items[0].total}',
                            style: const TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
