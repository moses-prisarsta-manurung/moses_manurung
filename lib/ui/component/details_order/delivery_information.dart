import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/utils/theme.dart';

class DeliveryInformation extends StatelessWidget {
  const DeliveryInformation(this.data, {super.key});

  final OrderModel data;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Padding(
          padding: EdgeInsets.only(top: 15.0, bottom: 15),
          child: Divider(
            color: AppTheme.color3,
            thickness: 5,
          ),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text('Delivery Information',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Row(
                children: const [
                  Padding(
                    padding: EdgeInsets.only(right: 70.0),
                    child: Text('Courier'),
                  ),
                  Text('JNT Express')
                ],
              ),
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 46.0),
                  child: Text('No. Receipt'),
                ),
                Text(data.order_key),
              ],
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 64.0),
                  child: Text('Address'),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          '${data.billing.first_name} ${data.billing.last_name}'),
                      Text(data.billing.phone),
                      Text(data.billing.email),
                      Text(
                        '${data.billing.address_1}, ${data.billing.address_2}, ${data.billing.city}, ${data.billing.state}, ${data.billing.country}, ${data.billing.postcode}',
                        softWrap: false,
                        maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ],
    );
  }
}
