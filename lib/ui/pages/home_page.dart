import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_scroll_to_top/flutter_scroll_to_top.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/ui/component/home/list_product_sale.dart';
import 'package:jubelio/ui/component/home/list_product.dart';
import 'package:jubelio/utils/theme.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(_) => GetBuilder<ProductController>(
      init: ProductController(),
      autoRemove: false,
      builder: (listProduct) {
        return NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (scrollstate) {
            if (listProduct.isDataLoading.isFalse) {
              listProduct.loadNextPage();
            }
            return false;
          },
          child: ScrollWrapper(
            promptAlignment: Alignment.bottomRight,
            promptAnimationCurve: Curves.easeInCirc,
            promptDuration: const Duration(milliseconds: 10),
            enabledAtOffset: 300,
            scrollOffsetUntilVisible: 100,
            alwaysVisibleAtOffset: true,
            promptTheme: const PromptButtonTheme(
                icon: Icon(
                  Icons.arrow_circle_up,
                  color: AppTheme.color4,
                ),
                color: AppTheme.color1,
                iconPadding: EdgeInsets.all(10),
                padding: EdgeInsets.all(20)),
            builder: (context, properties) => RefreshIndicator(
              onRefresh: (() async {
                Get.find<ProductController>().onInit();
              }),
              child: ListView(
                children: <Widget>[
                  new Container(
                    height: 300,
                    child: const NewListProduct(),
                  ),
                  const Padding(
                    padding: EdgeInsets.fromLTRB(16, 12, 12, 0),
                    child: Text(
                      'New For You',
                      style: TextStyle(
                          color: AppTheme.color1,
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  const ListProduct()
                ],
              ),
            ),
          ),
        );
      });
}
