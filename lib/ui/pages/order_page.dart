import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_scroll_to_top/flutter_scroll_to_top.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/order_controller.dart';
import 'package:jubelio/ui/component/order/list_order.dart';
import 'package:jubelio/utils/theme.dart';

class OrderPage extends StatelessWidget {
  const OrderPage({super.key});

  @override
  Widget build(_) => GetBuilder<OrderController>(
      init: OrderController(),
      autoRemove: false,
      builder: (listProduct) {
        return NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (scrollstate) {
            if (listProduct.isDataLoading.isFalse) {
              listProduct.loadNextPage();
            }
            return false;
          },
          child: ScrollWrapper(
            promptAlignment: Alignment.bottomRight,
            promptAnimationCurve: Curves.easeInCirc,
            promptDuration: const Duration(milliseconds: 10),
            enabledAtOffset: 300,
            scrollOffsetUntilVisible: 100,
            alwaysVisibleAtOffset: true,
            promptTheme: const PromptButtonTheme(
                icon: Icon(
                  Icons.arrow_circle_up,
                  color: AppTheme.color4,
                ),
                color: AppTheme.color1,
                iconPadding: EdgeInsets.all(10),
                padding: EdgeInsets.all(20)),
            builder: (context, properties) => RefreshIndicator(
              onRefresh: (() async {
                Get.find<OrderController>().onInit();
              }),
              child: ListView(
                children: <Widget>[const ListOrder()],
              ),
            ),
          ),
        );
      });
}
