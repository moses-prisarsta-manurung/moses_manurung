import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:jubelio/ui/component/home/search_bar.dart';
import 'package:jubelio/ui/pages/order_page.dart';
import 'package:jubelio/ui/pages/home_page.dart';
import 'package:jubelio/ui_logic/greetings.dart';
import 'package:jubelio/utils/theme.dart';

class RootPage extends StatelessWidget {
  RootPage({super.key});

  final greetings = greetingMessage();

  @override
  Widget build(BuildContext context) {
    return GetX<GetTabContoller>(
      init: GetTabContoller(),
      builder: (tc) {
        var botBar = BottomNavigationBar(
          backgroundColor: AppTheme.color8,
          unselectedItemColor: AppTheme.color2,
          unselectedLabelStyle: const TextStyle(color: Colors.black12),
          selectedItemColor: AppTheme.color3,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.camera_outlined), label: 'Cart'),
          ],
          currentIndex: tc.selectedIndex.value,
          onTap: tc.onItemTapped,
        );
        return Scaffold(
          backgroundColor: Color.fromARGB(255, 244, 247, 241),
          appBar: tc.selectedIndex.value == 0
              ? AppBar(
                  automaticallyImplyLeading: false,
                  title: Text(
                    'Hi, $greetings',
                    style: const TextStyle(color: AppTheme.color8),
                  ),
                  actions: const [SearchBar()],
                  backgroundColor: Colors.white12,
                  elevation: 0,
                )
              : null,
          body: Center(
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: tc.tabController,
              children: tc.widgetOptions,
            ),
          ),
          bottomNavigationBar: botBar,
        );
      },
    );
  }
}

class GetTabContoller extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;
  final RxInt selectedIndex = 0.obs;

  final List<Widget> widgetOptions = <Widget>[
    HomePage(),
    const OrderPage(),
  ];

  void onItemTapped(int index) {
    tabController.animateTo(index);
    selectedIndex.value = index;
  }

  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    super.onInit();
  }
}
