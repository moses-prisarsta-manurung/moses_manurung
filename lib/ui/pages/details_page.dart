import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/controller/product_controller.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/ui/component/details/book_bar.dart';
import 'package:jubelio/ui/component/details/details_content.dart';
import 'package:jubelio/utils/theme.dart';

class DetailsPage extends StatelessWidget {
  const DetailsPage(this.data, this.related, {super.key});

  final ProductModel data, related;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: AppTheme.backButton,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white12,
        elevation: 0,
      ),
      body: DetailsContent(data, related),
      bottomNavigationBar: BookBar(data),
    );
  }
}
