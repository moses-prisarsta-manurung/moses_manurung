import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_scroll_to_top/flutter_scroll_to_top.dart';
import 'package:get/get.dart';
import 'package:jubelio/ui/component/home/list_product.dart';
import 'package:jubelio/ui/component/home/list_product_by_search.dart';
import 'package:jubelio/ui/component/home/search_bar.dart';
import 'package:jubelio/utils/theme.dart';

import '../../business_logic/controller/product_controller.dart';

class SearchPage extends StatelessWidget {
  const SearchPage(this.keyword, {super.key});

  final TextEditingController keyword;

  Widget build(_) => GetBuilder<ProductController>(
      init: ProductController(),
      autoRemove: false,
      builder: (listProduct) {
        return Scaffold(
          appBar: AppBar(
            leading: AppTheme.backButton,
            automaticallyImplyLeading: false,
            actions: const [SearchBar()],
            backgroundColor: Colors.white12,
            elevation: 0,
          ),
          body: NotificationListener<OverscrollIndicatorNotification>(
            onNotification: (scrollstate) {
              if (listProduct.isDataLoading.isFalse) {
                listProduct.loadNextPage();
              }
              return false;
            },
            child: ScrollWrapper(
              promptAlignment: Alignment.bottomRight,
              promptAnimationCurve: Curves.easeInCirc,
              promptDuration: const Duration(milliseconds: 10),
              enabledAtOffset: 300,
              scrollOffsetUntilVisible: 100,
              alwaysVisibleAtOffset: true,
              promptTheme: const PromptButtonTheme(
                  icon: Icon(
                    Icons.arrow_circle_up,
                    color: AppTheme.color4,
                  ),
                  color: AppTheme.color1,
                  iconPadding: EdgeInsets.all(10),
                  padding: EdgeInsets.all(20)),
              builder: (context, properties) => RefreshIndicator(
                onRefresh: (() async {
                  Get.find<ProductController>().onInit();
                }),
                child: ListView(
                  children: <Widget>[ListProductBySearch(keyword)],
                ),
              ),
            ),
          ),
        );
      });
}
