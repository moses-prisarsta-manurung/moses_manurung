import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:get/get.dart';
import 'package:jubelio/business_logic/model/product_model.dart';
import 'package:jubelio/ui/component/confirm/confirm_bar.dart';
import 'package:jubelio/ui/component/confirm/confirm_content.dart';
import 'package:jubelio/ui/component/details/dialog.dart';
import 'package:jubelio/utils/theme.dart';

class ConfirmPage extends StatelessWidget {
  const ConfirmPage(this.data, this.count, {super.key});

  final ProductModel data;
  final int count;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: AppTheme.backButton,
          automaticallyImplyLeading: false,
          backgroundColor: Colors.white12,
          elevation: 0,
        ),
        body: ListView(
          children: [
            ConfirmContent(data, count),
          ],
        ),
        bottomNavigationBar: BottomConfirmBar());
  }
}
