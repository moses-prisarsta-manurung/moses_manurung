import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:jubelio/business_logic/model/order_model.dart';
import 'package:jubelio/ui/component/details_order/delivery_information.dart';
import 'package:jubelio/ui/component/details_order/payment_details.dart';
import 'package:jubelio/ui/component/details_order/product_details_order.dart';
import 'package:jubelio/utils/theme.dart';

class DetailsOrderPage extends StatelessWidget {
  const DetailsOrderPage(this.data, {super.key});

  final OrderModel data;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: AppTheme.backButton,
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white12,
        elevation: 0,
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(14.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ProductDetailsOrder(data),
                DeliveryInformation(data),
                PaymentDetails(data)
              ],
            ),
          )
        ],
      ),
    );
  }
}
