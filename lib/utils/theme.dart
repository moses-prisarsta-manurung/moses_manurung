import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppTheme {
  static const color1 = Color.fromARGB(255, 51, 51, 51);
  static const color2 = Color.fromARGB(255, 155, 168, 168);
  static const color3 = Color.fromARGB(255, 235, 235, 231);
  static const color4 = Color.fromARGB(255, 200, 206, 196);
  static const color5 = Color.fromARGB(255, 235, 235, 231);
  static const color6 = Color.fromARGB(255, 51, 51, 51);
  static const color7 = Color.fromARGB(255, 81, 110, 111);
  static const color8 = Color.fromARGB(255, 106, 123, 94);

  static BoxDecoration boxdecoration1 = BoxDecoration(
      color: Colors.white,
      boxShadow: const [
        BoxShadow(
          blurRadius: 4,
          color: Color(0x320E151B),
          offset: Offset(0, 1),
        )
      ],
      borderRadius: BorderRadius.circular(12));

  static const text18Bold =
      TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

  static BoxDecoration boxdecoration2 = BoxDecoration(
      color: AppTheme.color8, borderRadius: BorderRadius.circular(2));

  static const text12Bold = TextStyle(
      fontSize: 12, fontWeight: FontWeight.bold, color: AppTheme.color3);

  static const text11 = TextStyle(
    height: 1,
    overflow: TextOverflow.ellipsis,
    fontSize: 11,
  );

  static const text14 = TextStyle(fontSize: 14);

  static BoxDecoration boxdecoration3 = BoxDecoration(
      color: AppTheme.color7, borderRadius: BorderRadius.circular(2));

  static const text14Bold = TextStyle(
      fontSize: 14, fontWeight: FontWeight.bold, color: AppTheme.color3);

  static const BackButton backButton = BackButton(
    color: AppTheme.color8,
  );

  static CarouselOptions carouselOptions = CarouselOptions(
    height: 250,
    aspectRatio: 16 / 9,
    viewportFraction: 0.8,
    initialPage: 0,
    enableInfiniteScroll: true,
    reverse: false,
    autoPlay: true,
    autoPlayInterval: const Duration(seconds: 3),
    autoPlayAnimationDuration: const Duration(milliseconds: 800),
    autoPlayCurve: Curves.fastOutSlowIn,
    enlargeCenterPage: true,
    enlargeFactor: 0.2,
    scrollDirection: Axis.horizontal,
  );
}
